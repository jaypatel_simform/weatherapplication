const request = require('request')

const geocode = (address, callback) => {

    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+address+'.json?limit=2&access_token=pk.eyJ1IjoiamF5cGF0ZWxzaW1mb3JtIiwiYSI6ImNreW50MmlyMDBmcGUyd245ZGdjc201d24ifQ.3pzSQt_3D0LRcocizyS6ew'

    request(url,(error,{body})=>{
         res = JSON.parse(body);
        if(error) callback(url,undefined)
        else if(!res.features.length){
            callback('unable to find locatin , try to find another location',undefined)
        }
        else{
            const data = res.features[0].center
            callback(undefined,{
                lat : data [1],
                lon : data[0],
                location : res.features[0].place_name
            })
        }
    })
}

module.exports = geocode