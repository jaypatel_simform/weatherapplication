const request = require('request')

const forcast = (lat, lon, callback) => {
    const url = 'http://api.weatherstack.com/current?access_key=2572bc5ad4947878c3da6ef8b3db2e4a&query=' + lat + ',' + lon

    request({ url, json: true }, (error, { body }) => {
        if (error) callback('unable to connect with url', undefined)
        else if (body.error) {
            callback('unable to find locatin , try to find another location', undefined)
        }
        else {
            data = 'Current weather in ' + body.location.name + ' is '+body.current.weather_descriptions+' And temperature is ' + body.current.temperature + ' but feels like ' + body.current.feelslike;
            icon = body.current.weather_icons;
            extraInfo = body.current;
            callback(undefined, {data,icon,extraInfo})
        }
    })
}
module.exports = forcast