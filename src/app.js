const express = require('express')
const path = require('path')
const redis = require("redis");
const redisPort = 6379
const client = redis.createClient({ host: "localhost", port: redisPort });

//log error to the console if any occurs
client.on("error", async (err) => {
    console.log("err", err);
});
client.on("connect", () => {
    console.log("##############connnected with redis#########################")
})

// (async ()=>{
//     await client.connect()
// })();
const hbs = require('hbs')
//const request = require('request')
const geocode = require('./utils/geocode')
const forcast = require('./utils/forcast')
const { I18n } = require('i18n')
const app = express()

i18n = require('i18n'),
    i18n.configure({
        // setup some locales - other locales default to en silently
        locales: ['ru', 'en', 'fr'],
        register: global,
        // sets a custom cookie name to parse locale settings from
        cookie: 'yourcookiename',

        // where to store json files - defaults to './locales'
        directory: __dirname + '/locales'
    });
app.use(i18n.init);
i18n.setLocale('fr')
const port = process.env.PORT || 3000
//define paths for Express config
const publicDir = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

//define handlebars engine and views locations
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

//setup static directory to server
app.use(express.static(publicDir))

app.get('', (req, res) => {
    //  res.send(res.__('Hello World , my name is jay patel'));
    console.log(111);
    res.render('index', {
        title: 'weather app',
        name: "jay patel",
        city: ['bharuch', 'surat', 'anand', 'ahmedabad', 'botad']
    })
})

app.get('/about', (req, res) => {
    i18n.init(req, res)
    res.render('about', {
        title: 'About me',
        content: "This is my weather website , here you can find weather by location..",
        name: "jay patel"
    })
})

app.get('/help', (req, res) => {
    res.render('help', { name: "jay k patel", title: "Help" })
})

app.get('/products', (req, res) => {
    if (!req.query.search) {
        return res.send({
            error: 'You must provide a search term'
        })
    }
    console.log(req.query)
    res.send({
        products: [req.query.search, req.query.rate]
    })
})
app.get('/weather', async (req, res) => {
    console.log("in weather");
    if (!req.query.address) {
        console.log("address", req.query.address)
        return res.send({
            error: 'You must provide a city info..'
        })
    }
    try {
        // await client.connect()
        console.log("111");
        // console.log("2")
      //  client.get(req.query.address, async (err, geoCode) => {
            // console.log("geocode", geoCode)
            // if (err) throw err
            // if (geoCode) {
            //     await forcast(geoCode.lat, geoCode.lon, (error, forcastData) => {
            //         if (error)
            //             return res.send({ error })
            //         res.send({
            //             forcast: forcastData.data,
            //             extraInfo: forcastData.extraInfo,
            //             icon: forcastData.icon,
            //             location: location,
            //             address: req.query.address
            //         })
            //     })
            // }   
            // else {
                geocode(req.query.address, (error, { lat, lon, location } = {}) => {
                    if (error) {
                        return res.send({ error })
                    }
                    // client.set(req.query.address, { lat, lon }); //JSON.stringify(jobs.data)
                    forcast(lat, lon, (error, forcastData) => {
                        if (error)
                            return res.send({ error })
                        res.send({
                            forcast: forcastData.data,
                            extraInfo: forcastData.extraInfo,
                            icon: forcastData.icon,
                            location: location,
                            address: req.query.address
                        })

                    })
                })
 //           }
        // })
    } catch (err) {
        console.log("ERrrr", err)
        res.send(err)
   }
})

app.get('/help/*', (req, res) => {
    res.render('notfound', { errorMessage: "Help artical not found", title: 404 })
})

app.get('*', (req, res) => {
    res.render('notfound', { errorMessage: "Page not found", title: 404 })
})

app.listen(port, () => {
    console.log(`server is running on : 127.0.0.1:${port}`)
})