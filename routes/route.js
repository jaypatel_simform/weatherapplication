const express = require('express')
const route = express.Router()
const geocode = require('../src/utils/geocode')
const forcast = require('../src/utils/forcast')
// const app = express()

route.get('', (req, res) => {
    res.render('index', {
        title: 'weather app',
        name: "jay patel",
        city: ['bharuch', 'surat', 'anand', 'ahmedabad', 'botad']
    })
})
route.get('/about', (req, res) => {
    i18n.init(req, res)
    res.render('about', {
        title: 'About me',
        content: "This is my weather website , here you can find weather by location..",
        name: "jay patel"
    })
})

route.get('/help', (req, res) => {
    res.render('help', { name: "jay k patel", title: "Help" })
})

route.get('/products', (req, res) => {
    if (!req.query.search) {
        return res.send({
            error: 'You must provide a search term'
        })
    }
    console.log(req.query)
    res.send({
        products: [req.query.search, req.query.rate]
    })
})
route.get('/weather', async (req, res) => {
    if (!req.query.address) {
        console.log("address", req.query.address)
        return res.send({
            error: 'You must provide a city info..'
        })
    }
    try {
        // await client.connect()
        // // console.log("2")
        // client.get(req.query.address, async (err, geoCode) => {
        //     console.log("geocode", geoCode)
        //     if (err) throw err
        //     if (geoCode) {
        //         await forcast(geoCode.lat, geoCode.lon, (error, forcastData) => {
        //             if (error)
        //                 return res.send({ error })
        //             res.send({
        //                 forcast: forcastData.data,
        //                 extraInfo: forcastData.extraInfo,
        //                 icon: forcastData.icon,
        //                 location: location,
        //                 address: req.query.address
        //             })
        //         })
        //     }
        //     else {
                geocode(req.query.address, (error, { lat, lon, location } = {}) => {
                    if (error) {
                        return res.send({ error })
                    }
                    // client.set(req.query.address, { lat, lon }); //JSON.stringify(jobs.data)
                    forcast(lat, lon, (error, forcastData) => {
                        if (error)
                            return res.send({ error })
                        res.send({
                            forcast: forcastData.data,
                            extraInfo: forcastData.extraInfo,
                            icon: forcastData.icon,
                            location: location,
                            address: req.query.address
                        })

                    })
                })
        //     }
        // })
    } catch (err) {
        console.log("ERrrr", err)
        res.send(err)
    }
})

route.get('/help/*', (req, res) => {
    res.render('notfound', { errorMessage: "Help artical not found", title: 404 })
})

route.get('*', (req, res) => {
    res.render('notfound', { errorMessage: "Page not found", title: 404 })

})

module.exports = route